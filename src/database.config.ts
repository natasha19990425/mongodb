export default {
  connectionString:
    process.env.MONGODB_CONNECTION_STRING || 'mongodb://localhost:27017',
  databaseName: process.env.MONGODB_DATABASE_NAME || 'mydatabase',
};
