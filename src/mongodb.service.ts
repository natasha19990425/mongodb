import { Injectable, OnModuleInit } from '@nestjs/common';
import { MongoClient, Db } from 'mongodb';
import databaseConfig from './database.config';

@Injectable()
export class MongodbService implements OnModuleInit {
  private client: MongoClient;
  private database: Db;

  async onModuleInit() {
    try {
      // 建立 MongoDB 連接
      this.client = await MongoClient.connect(databaseConfig.connectionString);

      console.log('Connected to MongoDB');

      // 選擇資料庫
      this.database = this.client.db(databaseConfig.databaseName);

      // 在此進行其他初始化操作，例如建立索引等
    } catch (error) {
      console.error('Failed to connect to MongoDB', error);
    }
  }

  getClient() {
    return this.client;
  }

  //新增了一個 getDatabase 方法，以便在需要的地方取得該資料庫實例。
  getDatabase() {
    return this.database;
  }
}
